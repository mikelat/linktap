# Linktap

To start server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To run tests:

  * Use `mix test`

## API Endpoints

Endpoints for API are as follows:

---

POST /api/v1/links

Expects POST data { url: "http://website.com" }

Returns { success: true, id: "slug",  short_url: "http://shortenedurl.com/go/slug", full_url: "http://website.com", errors: [] }

---

GET /api/v1/links/slug

Returns {id: "slug", short_url: "http://shortenedurl.com/go/slug", full_url: "http://website.com", click_count: "1", clicks: [ (click data) ] }

---