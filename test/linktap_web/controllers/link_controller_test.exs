defmodule LinktapWeb.LinkControllerTest do
  use LinktapWeb.ConnCase

  alias Linktap.Links

  @create_attrs %{url: "http://www.google.ca"}
  @invalid_attrs %{click_count: nil, slug: nil, url: nil}

  def fixture(:link) do
    {:ok, link} = Links.create_link(@create_attrs)
    link
  end

  def api_prefix do
    "/api/v1"
  end

  describe "new link" do
    test "renders form", %{conn: conn} do
      conn = get conn, link_path(conn, :new)
      assert html_response(conn, 200) =~ "enter url"
    end
  end

  describe "create link" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, link_path(conn, :create), link: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == link_path(conn, :show, id)

      conn = get conn, link_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Short URL"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, link_path(conn, :create), link: @invalid_attrs
      assert html_response(conn, 200) =~ "enter url"
    end
  end

  describe "create link api" do
    test "return success when data is valid", %{conn: conn} do
      conn = post conn, api_prefix() <> link_path(conn, :create), link: @create_attrs
      resp = json_response(conn, 200)
      assert resp["success"] == true
    end

    test "return analytics", %{conn: conn} do
      conn = post conn, api_prefix() <> link_path(conn, :create), link: @create_attrs
      resp = json_response(conn, 200)
      assert resp["success"] == true

      conn = get conn, api_prefix() <> link_path(conn, :show, resp["id"])
      show_resp = json_response(conn, 200)
      assert resp["id"] == show_resp["id"]
    end

    test "return errors when data is invalid", %{conn: conn} do
      conn = post conn, api_prefix() <> link_path(conn, :create), link: @invalid_attrs
      resp = json_response(conn, 200)
      assert resp["success"] == false
    end
  end

  describe "redirection" do
    test "redirect appropriately and record click", %{conn: conn} do
      conn = post conn, api_prefix() <> link_path(conn, :create), link: @create_attrs
      resp = json_response(conn, 200)

      conn = get conn, resp["short_url"]
      assert redirected_to(conn) == resp["full_url"]

      conn = get conn, api_prefix() <> link_path(conn, :show, resp["id"])
      show_resp = json_response(conn, 200)
      assert Enum.count(show_resp["clicks"]) == 1
    end
  end


end
