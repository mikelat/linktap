# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :linktap,
  ecto_repos: [Linktap.Repo],
  hash_ids_salt: "this_would_be_secret"

# Configures the endpoint
config :linktap, LinktapWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "QVC2j3nDrqC4mXdB9mtf54LaesTNky0qEhuy6Bgy1FK0ySNV3x+GJsnLe+YcDXfw",
  render_errors: [view: LinktapWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Linktap.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
