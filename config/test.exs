use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :linktap, LinktapWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
# config :linktap, Linktap.Repo,
#   adapter: Ecto.Adapters.Postgres,
#   username: "postgres",
#   password: "postgres",
#   database: "linktap_test",
#   hostname: "localhost",
#   pool: Ecto.Adapters.SQL.Sandbox

config :linktap, Linktap.Repo,
  adapter: Sqlite.Ecto2,
  database: "db/linktap_test.sqlite3",
  pool: Ecto.Adapters.SQL.Sandbox