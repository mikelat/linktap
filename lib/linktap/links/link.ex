defmodule Linktap.Links.Link do
  use Ecto.Schema
  import Ecto.Changeset
  alias Linktap.Links.Link
  alias Linktap.Click


  schema "links" do
    field :url, :string
    field :slug, :string
    field :click_count, :integer, default: 0

    has_many :clicks, Click

    timestamps()
  end

  @doc false
  def changeset(%Link{} = link, attrs) do
    link
    |> cast(attrs, [:url])
    |> validate_required([:url])
    |> validate_url()
  end

  def validate_url(link) do
    result =
      case URI.parse(get_change(link, :url, "")) do
        %URI{scheme: nil} -> :error
        %URI{host: nil}   -> :error
        _                 -> :ok
      end

    if result == :error && link.errors == [] do
      add_error(link, :url, "is invalid")
    else
      link
    end
  end
end
