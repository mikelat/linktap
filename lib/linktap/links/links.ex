defmodule Linktap.Links do
  @moduledoc """
  The Links context.
  """

  import Ecto.Query, warn: false
  alias Linktap.Repo
  alias Linktap.Links.Link
  alias Linktap.Click

  @hash_ids Hashids.new([
    salt: Application.get_env(:linktap, :hash_ids_salt),
    min_len: 5,
  ])

  @doc """
  Returns the list of links.

  ## Examples

      iex> list_links()
      [%Link{}, ...]

  """
  def list_links do
    Repo.all(Link)
  end

  @doc """
  Gets a single link.

  Raises `Ecto.NoResultsError` if the Link does not exist.

  ## Examples

      iex> get_link!(123)
      %Link{}

      iex> get_link!(456)
      ** (Ecto.NoResultsError)

  """
  def get_link!(slug) do
    {:ok, id} = Hashids.decode(@hash_ids, slug)
    Repo.get!(Link, List.last(id))
  end

  @doc """
  Creates a link.

  ## Examples

      iex> create_link(%{field: value})
      {:ok, %Link{}}

      iex> create_link(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_link(attrs \\ %{}) do
    db_result =
      %Link{}
      |> Link.changeset(attrs)
      |> Repo.insert()

    {result, link} = db_result
    db_result =
      if result == :ok do
        link
        |> Ecto.Changeset.change(%{slug: Hashids.encode(@hash_ids, link.id)})
        |> Repo.update()
      else
        db_result
      end

    db_result
  end

  @doc """
  Updates a link.

  ## Examples

      iex> update_link(link, %{field: new_value})
      {:ok, %Link{}}

      iex> update_link(link, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_link(%Link{} = link, attrs) do
    link
    |> Link.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Link.

  ## Examples

      iex> delete_link(link)
      {:ok, %Link{}}

      iex> delete_link(link)
      {:error, %Ecto.Changeset{}}

  """
  def delete_link(%Link{} = link) do
    Repo.delete(link)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking link changes.

  ## Examples

      iex> change_link(link)
      %Ecto.Changeset{source: %Link{}}

  """
  def change_link(%Link{} = link) do
    Link.changeset(link, %{})
  end

  def increment_click(link) do
    from(l in Link, update: [inc: [click_count: 1]], where: l.id == ^link.id)
    |> Repo.update_all([])
  end

  def add_click(link, conn) do
    {:ok, _} = %Click{}
      |> Click.changeset(%{
          link_id: link.id,
          ip_address: get_ip_address(conn),
          device: "#{Plug.Conn.get_req_header(conn, "user-agent")}",
          referrer: "#{Plug.Conn.get_req_header(conn, "referrer")}"
        })
      |> Repo.insert()

    increment_click(link)
  end

  # Fix for cloudflare IP
  defp get_ip_address(conn) do
    ip = "#{Plug.Conn.get_req_header(conn, "cf-connecting-ip")}"
    if ip == "" do
      to_string(:inet_parse.ntoa(conn.remote_ip))
    else
      ip
    end
  end
end
