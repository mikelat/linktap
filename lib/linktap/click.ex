defmodule Linktap.Click do
  use Ecto.Schema
  import Ecto.Changeset
  alias Linktap.Click


  schema "clicks" do
    field :device, :string
    field :ip_address, :string
    field :referrer, :string

    belongs_to :link, Linktap.Links.Link

    timestamps()
  end

  @doc false
  def changeset(%Click{} = click, attrs) do
    click
    |> cast(attrs, [:ip_address, :device, :referrer, :link_id])
  end
end
