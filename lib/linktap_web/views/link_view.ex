defmodule LinktapWeb.LinkView do
  use LinktapWeb, :view

  def get_short_url(conn, link) do
    port = if conn.port == 80, do: "", else: ":#{conn.port}"
    site_url = Application.get_env(:linktap, :site_url, "#{conn.scheme}://#{conn.host}#{port}")
    "#{site_url}/go/#{link.slug}"
  end

  def render("show.json", %{conn: conn, link: link}) do
    clicks =
      Enum.map(link.clicks, fn(x) ->
        Map.from_struct(x)
        |> Map.take([:ip_address, :device, :inserted_at, :referrer])
      end)

    %{id: link.slug, short_url: get_short_url(conn, link), full_url: link.url, click_count: link.click_count, clicks: clicks }
  end

  def render("new.json", %{conn: conn, link: link}) do
    %{success: true, id: link.slug, short_url: get_short_url(conn, link), full_url: link.url, errors: [] }
  end

  def render("new.json", %{changeset: changeset}) do
    %{success: false, id: nil,  short_url: nil, full_url: nil, errors: error_json(changeset, :url) }
  end

end
