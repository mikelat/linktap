defmodule LinktapWeb.Router do
  use LinktapWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LinktapWeb do
    pipe_through :browser # Use the default browser stack

    resources "/links", LinkController, only: [:new, :create, :show]

    get "/go/:id", LinkController, :go
    get "/", LinkController, :new
  end

  scope "/api/v1", LinktapWeb do
    pipe_through :api

    resources "/links", LinkController, only: [:create, :show]
  end
end
