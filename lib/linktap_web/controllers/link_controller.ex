defmodule LinktapWeb.LinkController do
  use LinktapWeb, :controller

  alias Linktap.Repo
  alias Linktap.Links
  alias Linktap.Links.Link

  def new(conn, _params) do
    changeset = Links.change_link(%Link{})
    render(conn, :new, changeset: changeset)
  end

  def create(conn, %{"link" => link_params}) do
    case Links.create_link(link_params) do
      {:ok, link} ->
        if get_format(conn) == "json" do
          render(conn, :new, link: link)
        else
          conn
          |> put_flash(:info, "Link created successfully.")
          |> redirect(to: link_path(conn, :show, link.slug))
        end
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    link =
      Links.get_link!(id)
      |> Repo.preload(:clicks)
    render(conn, :show, link: link)
  end

  # Record and redirect
  def go(conn, %{"id" => id}) do
    link = Links.get_link!(id)
    Links.add_click(link, conn)
    redirect(conn, external: link.url)
  end

end
