defmodule Linktap.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :url, :string
      add :slug, :string
      add :click_count, :integer, default: 0

      timestamps()
    end

  end
end
