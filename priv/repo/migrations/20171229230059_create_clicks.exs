defmodule Linktap.Repo.Migrations.CreateClicks do
  use Ecto.Migration

  def change do
    create table(:clicks) do
      add :ip_address, :string
      add :device, :string
      add :referrer, :string

      add :link_id, references(:links)

      timestamps()
    end

  end
end
